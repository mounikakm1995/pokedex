package com.example.pokedex;

public interface CardClickListener {
    void openNewActivity(String name);
}
