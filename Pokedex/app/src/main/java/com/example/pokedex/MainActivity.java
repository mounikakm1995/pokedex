package com.example.pokedex;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<String> namesList;
    PokemonAdapter pokemonAdapter;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView=findViewById(R.id.recyclerView);
        progress=findViewById(R.id.progress);
        LinearLayoutManager layoutManager=new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        namesList=new ArrayList<>();
        getData("https://pokeapi.co/api/v2/pokemon");
    }






    private void getData(String Url) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Url ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response){
                        try {
                            Log.e("Response",response);
                            progress.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            JSONObject data=new JSONObject(response);
                           JSONArray results= data.getJSONArray("results");

                           for(int i=0;i<results.length();i++){
                               String name=results.getJSONObject(i).getString("name");
                               namesList.add(name);
                           }
                            pokemonAdapter=new PokemonAdapter(namesList);
                           recyclerView.setAdapter(pokemonAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("api", error.getMessage().toString());
                progress.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this,"Somthing went wrong",Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this) ;
        queue.add(stringRequest) ;
    }
}
