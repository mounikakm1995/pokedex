package com.example.pokedex;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder> {


    private ArrayList<String> nameList;
    private Context mContext;


    public PokemonAdapter(ArrayList<String> nameList){

        this.nameList=nameList;



    }
    @NonNull
    @Override
    public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext=parent.getContext();
        int layoutId= R.layout.itemlayout;
        LayoutInflater layoutInflater=LayoutInflater.from(mContext);
        View view=layoutInflater.inflate(layoutId,parent,false);
        return new PokemonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonViewHolder holder, int position) {

    final String name=nameList.get(position);
    holder.textView.setText(name);
    holder.cardView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent(mContext,ResultActivity.class);
            intent.putExtra("name",name);
            mContext.startActivity(intent);
            //Toast.makeText(mContext,"Clicked",Toast.LENGTH_SHORT).show();
        }
    });


    }

    @Override
    public int getItemCount() {
        if(nameList.size()>0){
            return nameList.size();

        }else {

            return 0;

        }
    }

    class PokemonViewHolder extends RecyclerView.ViewHolder{


        TextView textView;
        CardView cardView;
        public PokemonViewHolder(View itemView)
        {
            super(itemView);
            textView=itemView.findViewById(R.id.textView);
            cardView=itemView.findViewById(R.id.cardView);


        }
    }
}
