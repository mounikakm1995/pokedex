package com.example.pokedex;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    String name="name";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        if(getIntent().getExtras()!=null){
            name =getIntent().getExtras().getString("name");
        }

        TextView nameTv=findViewById(R.id.resultText);
        nameTv.setText(name);

    }
}
